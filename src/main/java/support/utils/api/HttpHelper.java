package support.utils.api;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.basePath;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

public class HttpHelper {

    private RequestSpecification requestSpecification;
    private Response response;

    public static final ContentType DEFAULT_CONTENT_TYPE = JSON;


    public HttpHelper(RequestSpecification requestSpecification) {
        this.requestSpecification = requestSpecification;
    }

    /**
     * *** Sets Base URI ***
     */
    public HttpHelper setBaseURI(String uri) {
        requestSpecification.baseUri(uri);
        return this;
    }

    /**
     * *** Sets Path of Endpoint ***
     * <p>
     * Provide path of endpoint (with parameters if needed),
     */
    public HttpHelper setPath(String uri) {
        requestSpecification = given().basePath(uri);
        return this;
    }

    /**
     * Verify the http response status returned. Check Status Code is 200?
     */
    public int getStatusCode() {
        return response.getStatusCode();
    }

    /**
     * *** Sets ContentType ***
     * <p>
     * We should set content type as JSON or XML before starting the test
     */
    public HttpHelper setContentType(ContentType contentType) {
        requestSpecification.contentType(contentType);
        return this;
    }

    /**
     * *** Returns JsonPath object ***
     * <p>
     * First convert the API's response to String type with "asString()" method.
     * Then, send this String formatted json response to the JsonPath class and return the JsonPath
     */
    public JsonPath getJsonPath() {
        String json = response.asString();
        System.out.print("Returned JSON: " + "\n" + json + "\n");
        return new JsonPath(json);
    }

    /**
     * *** Sets header ***
     * <p>
     * Sets header for requests
     *
     * @param headerName  The header name
     * @param headerValue The header value
     */
    public HttpHelper header(String headerName, String headerValue) {
        requestSpecification.header(headerName, headerValue);
        return this;
    }

    /**
     * *** Sets setBody ***
     * <p>
     * For POST requestSpecification sets setBody with default content type JSON
     */
    public HttpHelper setBody(Object parameters) {
        requestSpecification.contentType(DEFAULT_CONTENT_TYPE).body(parameters);
        return this;
    }

    /**
     * *** Sets setBody ***
     * <p>
     * For POST requestSpecification sets setBody
     * Set JSON or XML as content Type
     */
    public HttpHelper setBody(Object parameters, ContentType contentType) {
        requestSpecification.contentType(contentType).body(parameters);
        return this;
    }

    /**
     * *** Sends POST ***
     * <p>
     * Sends POST requestSpecification
     */
    public HttpHelper post() {
        response = requestSpecification.post();
        return this;
    }

    /**
     * *** Sends POST with path parameter(s) ***
     * <p>
     * Sends POST requestSpecification with parameters in path
     *
     * @param pathParameters - parameter(s) for endpoint path
     */
    public HttpHelper post(Object... pathParameters) {
        response = requestSpecification.when().post(basePath, pathParameters);
        return this;
    }


    /**
     * *** Sends GET ***
     * <p>
     * Sends GET request
     */
    public HttpHelper get() {
        response = requestSpecification.get();
        return this;
    }

    /**
     * *** Sends GET with path parameter(s) ***
     * <p>
     * Sends GET requestSpecification with parameters in path
     *
     * @param pathParameters - parameter(s) for endpoint path
     */
    public HttpHelper get(Object... pathParameters) {
        response = requestSpecification.when().get(basePath, pathParameters);
        return this;
    }


    /**
     * The response of a requestSpecification made by REST Assured.
     * <p>
     * Usage example:
     * Response response = get("/lotto");
     * String setBody = response.getBody().asString();
     * String headerValue = response.getHeader("headerName");
     * String cookieValue = response.getCookie("cookieName");
     *
     * @return Response object
     */
    public Response getResponse() {
        return response;
    }
}