package support.utils.api;

import io.restassured.response.Response;
import org.jetbrains.annotations.Nullable;

import static support.endpoints.Endpoints.*;

public class ApiMethods {

    private HttpHelper httpHelper;

    public ApiMethods(HttpHelper httpHelper) {
        this.httpHelper = httpHelper;
    }

    /**
     * Sends POST request to /diffassign/v1/diff/{id}/{side} endpoint
     *
     * @param id          - value to be used to get set value via getDiff endpoint
     * @param sideValue   - could be left or right
     * @param base64Value - Base64 encoded value
     * @return * The response of a requestSpecification made by REST Assured.
     * * <p>
     * * Usage example:
     * * Response response = get("/lotto");
     * * String setBody = response.getBody().asString();
     * * String headerValue = response.getHeader("headerName");
     * * String cookieValue = response.getCookie("cookieName");
     */
    public Response setSideValue(String id, String sideValue, @Nullable String base64Value) {
        httpHelper.setPath(SET_SIDE_VALUE);
        if (base64Value != null) httpHelper.setBody(String.format("\"%s\"", base64Value));
        httpHelper.post(id, sideValue);

        return httpHelper.getResponse();
    }

    /**
     * Sends GET request to /diffassign/v1/diff/{id}/ endpoint
     *
     * @param id - value that was used to set values via setSideValue endpoint
     * @return * The response of a requestSpecification made by REST Assured.
     * * <p>
     * * Usage example:
     * * Response response = get("/lotto");
     * * String setBody = response.getBody().asString();
     * * String headerValue = response.getHeader("headerName");
     * * String cookieValue = response.getCookie("cookieName");
     */
    public Response getDiff(@Nullable String id) {
        httpHelper.setPath(id == null ? GET_DIFF_NO_ID : GET_DIFF);
        if (id == null) {
            httpHelper.get();
        } else {
            httpHelper.get(id);
        }

        return httpHelper.getResponse();
    }

    public HttpHelper getHttpHelper() {
        return httpHelper;
    }
}
