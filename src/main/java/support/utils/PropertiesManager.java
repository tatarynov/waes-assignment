package support.utils;


import java.util.Properties;

public class PropertiesManager {

    private Properties properties = new Properties();
    private static PropertiesManager instance;

    public final static String ENV_PROPERTY = "env";

    private PropertiesManager() {
//		some stuff to set if needed, e.g.:

//		InputStream inputStream;
//		try {
//			String resource = Thread.currentThread().getContextClassLoader().getResource("config.properties").getPath();
//			inputStream = new FileInputStream(resource);
//			properties.load(inputStream);
//		} catch (java.io.IOException e) {
//			e.printStackTrace();
//		}
    }

    static PropertiesManager getInstance() {
        if (instance == null) instance = new PropertiesManager();
        return instance;
    }

    /**
     * *** Retrieves environment property ***
     *
     * @return String value of -Denv property
     */
    String getEnvProperty() {
        return System.getProperty(ENV_PROPERTY, "").toLowerCase();
    }

}
