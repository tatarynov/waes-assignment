package support.utils;

public class EnvManager {
    private static EnvManager ourInstance = new EnvManager();

    public static EnvManager getInstance() {
        return ourInstance;
    }

    private static final String LOCAL_ENV = "http://localhost";
    private static final String TEST_ENV = "http://example";
    private static final String STAGE_ENV = "http://example";
    private static final String PROD_ENV = "http://example";

    private static final String DEFAULT_PORT = "8081";

    private EnvManager() {
    }

    /**
     * *** Retrieves environment url for different servers ***
     * <p>
     * By default equals 'http://localhost'
     */
    public String getEnvUrl() {
        String envProperty = PropertiesManager.getInstance().getEnvProperty();
        String envUrl = LOCAL_ENV;
        if (envProperty.contains("test")) {
            envUrl = TEST_ENV;
        } else if (envProperty.contains("stage")) {
            envUrl = STAGE_ENV;
        } else if (envProperty.contains("prod")) {
            envUrl = PROD_ENV;
        }
        return envUrl.concat(":").concat(DEFAULT_PORT);
    }
}
