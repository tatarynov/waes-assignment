package support.dto;

import java.util.HashMap;

public class Body extends HashMap<BodyValue, String> {

    public String get(BodyValue key) {
        return super.get(key.toString());
    }
}
