package support.dto;

public enum BodyValue {
    MIN_LENGTH,
    MAX_LENGTH,
    MORE_THAN_ALLOWED,
    ONLY_NUMBERS,
    ONLY_CHARS,
    WITH_ONE_EQUAL_SIGNS,
    WITH_TWO_EQUAL_SIGNS
}
