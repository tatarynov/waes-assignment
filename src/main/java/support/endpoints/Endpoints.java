package support.endpoints;

public class Endpoints {

    public static final String SET_SIDE_VALUE = "/diffassign/v1/diff/{id}/{side}";
    public static final String GET_DIFF = "/diffassign/v1/diff/{id}/";

    public static final String GET_DIFF_NO_ID = "/diffassign/v1/diff/";
}
