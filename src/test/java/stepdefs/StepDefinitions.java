package stepdefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import support.utils.api.ApiMethods;
import utils.TestData;

import static org.assertj.core.api.Assertions.assertThat;
import static support.endpoints.Endpoints.SET_SIDE_VALUE;

public class StepDefinitions {

    private ApiMethods api = Configuration.getApi();
    private Response response;

    @Given("^Send POST request to /diffassign/v1/diff/\"([^\"]*)\"/\"([^\"]*)\" endpoint with value \"([^\"]*)\"$")
    public void sendPOSTRequestToDiffassignVDiffEndpointWithValue(String id, String sideValue, String value) {
        value = TestData.getBodyValue(value);
        response = api.setSideValue(id, sideValue, value);
    }

    @When("^Send GET request to /diffassign/v1/diff/\"([^\"]*)\"/ endpoint$")
    public void sendGETRequestToDiffassignVDiffEndpoint(String id) {
        if (id.equals("NO_ID")) {
            response = api.getDiff(null);
        } else {
            response = api.getDiff(id);
        }
    }

    @Then("^Response must contain \"([^\"]*)\" in body$")
    public void responseShouldContainInBody(String arg0) {
        assertThat(response.body().asString()).contains(arg0);
    }

    @And("Response must contain \"(.*)\" status code")
    public void responseMustContainStatusCode(int statusCode) {
        assertThat(response.statusCode()).isEqualTo(statusCode);
    }

    @And("^Response must contain \"(.*)\" and \"([^\"]*)\" in body$")
    public void responseMustContainAndInBody(String value1, String value2) {
        value1 = TestData.getBodyValue(value1);
        value2 = TestData.getBodyValue(value2);

        assertThat(response.body().asString()).contains(value1);
        assertThat(response.body().asString()).contains(value2);
    }

    @Given("^Send POST request to /diffassign/v1/diff/\"([^\"]*)\"/\"([^\"]*)\" endpoint with value \"([^\"]*)\" with \"([^\"]*)\" Media-Type$")
    public void sendPOSTRequestToDiffassignVDiffEndpointWithValueWithMediaType(String id, String sideValue, String value, String mediaType) {
        ContentType contentType = ContentType.valueOf(mediaType);
        value = TestData.getBodyValue(value);
        response = api
                .getHttpHelper()
                .setContentType(contentType)
                .setBody(value)
                .setPath(SET_SIDE_VALUE)
                .post(id, sideValue)
                .getResponse();
    }
}
