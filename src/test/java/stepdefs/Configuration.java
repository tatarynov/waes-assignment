package stepdefs;

import cucumber.api.java.Before;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.ErrorLoggingFilter;
import support.utils.EnvManager;
import support.utils.api.ApiMethods;
import support.utils.api.HttpHelper;

import static support.utils.api.HttpHelper.DEFAULT_CONTENT_TYPE;

public class Configuration {

    private static ThreadLocal<HttpHelper> httpHelper = new ThreadLocal<>();
    private static ThreadLocal<ApiMethods> api = new ThreadLocal<>();

    @Before
    public synchronized void initializeTest() {
        RestAssured.requestSpecification = new RequestSpecBuilder()
//                .addFilter(ErrorLoggingFilter.errorLogger()) // A filter that'll print the body if an error occurred (status code is between 400 and 500)
                .setBaseUri(EnvManager.getInstance().getEnvUrl())
                .setContentType(DEFAULT_CONTENT_TYPE)
                .build();

        httpHelper.set(new HttpHelper(RestAssured.requestSpecification));
        api.set(new ApiMethods(httpHelper.get()));
    }

    public static HttpHelper getHttpHelper() {
        return httpHelper.get();
    }

    static ApiMethods getApi() {
        return api.get();
    }
}
