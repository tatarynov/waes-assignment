package utils;

import com.esotericsoftware.yamlbeans.YamlReader;
import org.jetbrains.annotations.Nullable;
import support.dto.Body;
import support.dto.BodyValue;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TestData {

    private final static String BODYVALUES_PATH = "src/test/resources/testdata/bodyvalues.yml";

    @Nullable
    public static String getBodyValue(String value) {
        BodyValue[] values = BodyValue.values();
        for (BodyValue bodyValue : values) {
            if (value.equals(bodyValue.toString())) return getBodyValue(bodyValue);
        }

        if (value.equals("NULL")) return null;
        return value;
    }

    /**
     * retrieves value for body from bodyvalues test data file
     *
     * @param bodyValue
     * @return
     */
    private static String getBodyValue(BodyValue bodyValue) {
        String bodyStringValue = "";
        String path = new File(BODYVALUES_PATH).getAbsolutePath();
        YamlReader doc = null;
        try {
            doc = new YamlReader(new FileReader(path));
            Body body = doc.read(Body.class);
            bodyStringValue = body.get(bodyValue);
        } catch (Exception e) {
            e.getLocalizedMessage();
            e.printStackTrace();
        } finally {
            if (doc != null) try {
                doc.close();
            } catch (IOException e) {
                // silently
            }
        }
        return bodyStringValue;
    }
}
