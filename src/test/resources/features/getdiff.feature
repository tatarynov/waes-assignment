@Diff @Smoke
Feature: Test GET /diffassign/v1/diff/{id} endpoint

  # @Assumptions for test:
  # Min body value length = 4 chars (meets current implementation)
  # Max body value length = 64 chars (doesn't meet current implementation)

  Scenario Outline: Compare valid Base64 values: with min, max values and value 'min + additional 4 chars' length
    Given Send POST request to /diffassign/v1/diff/"<id>"/"left" endpoint with value "<value1>"
    And Send POST request to /diffassign/v1/diff/"<id>"/"right" endpoint with value "<value2>"
    When Send GET request to /diffassign/v1/diff/"<id>"/ endpoint
    Then Response must contain "<result>" in body
    And Response must contain "200" status code
    Examples:
      | id | value1     | value2     | result           |
      | 1  | MIN_LENGTH | MIN_LENGTH | EQUAL            |
      | 2  | MAX_LENGTH | MAX_LENGTH | EQUAL            |
      | 3  | MIN_LENGTH | MTIzSGVs   | DIFFERENT_LENGTH |
      | 4  | MIN_LENGTH | MAX_LENGTH | DIFFERENT_LENGTH |


  Scenario Outline: Compare updated values for both sides: for equal values and values with different length
    Given Send POST request to /diffassign/v1/diff/"<id>"/"left" endpoint with value "<value1>"
    And Send POST request to /diffassign/v1/diff/"<id>"/"right" endpoint with value "<value2>"
    When Send GET request to /diffassign/v1/diff/"<id>"/ endpoint
    Then Response must contain "<result>" in body
    And Response must contain "200" status code
    Examples:
      | id | value1     | value2       | result           |
      | 1  | MIN_LENGTH | MIN_LENGTH   | EQUAL            |
      | 1  | MAX_LENGTH | MAX_LENGTH   | EQUAL            |
      | 1  | MTIzSGVs   | VGVzdCBXQUVT | DIFFERENT_LENGTH |


  Scenario Outline: Compare different side values with the same length but differ by: single, in range and randomly placed chars
    Given Send POST request to /diffassign/v1/diff/"<id>"/"left" endpoint with value "<value1>"
    And Send POST request to /diffassign/v1/diff/"<id>"/"right" endpoint with value "<value2>"
    When Send GET request to /diffassign/v1/diff/"<id>"/ endpoint
    Then Response must contain "<result1>" in body
    And Response must contain "<result2>" in body
    And Response must contain "200" status code
    Examples:
      | id | value1       | value2       | result1         | result2                                           |
      | 11  | VGVzdCBXQUVT | MGVzdCBXQUVT | DIFFERENT_CHARS | Values are different on char(s) [0].              |
      | 12  | VGVzdCBXQUVT | SLMsbG8sIFdv | DIFFERENT_CHARS | Values are different on char(s) [0-11].           |
      | 13  | VGVzdCBXQUVT | V2VsbG8XQFdv | DIFFERENT_CHARS | Values are different on char(s) [1] [3-6] [9-11]. |


  Scenario Outline: Compare side values with no set values for left or right side
    Given Send POST request to /diffassign/v1/diff/"<id>"/"<side>" endpoint with value "<value1>"
    When Send GET request to /diffassign/v1/diff/"<id>"/ endpoint
    Then Response must contain "200" status code
    And Response must contain "<result1>" in body
    And Response must contain "<result2>" in body
    Examples:
      | id   | side  | value1       | result1          | result2                       |
      | 1000 | left  | VGVzdCBXQUVT | DIFFERENT_LENGTH | Right side contains no value. |
      | 1001 | right | VGVzdCBXQUVT | DIFFERENT_LENGTH | Left side contains no value.  |


  Scenario: Compare side values with no set values for both sides
    When Send GET request to /diffassign/v1/diff/"1003"/ endpoint
    Then Response must contain "404" status code
    And Response must contain "ID 1003 not initialized." in body
    And Response must contain "errorCode" and "404" in body


  Scenario: Send request with no specified ID
    Given Send POST request to /diffassign/v1/diff/"1005"/"left" endpoint with value "VGhpcyBpcyB0ZXN0IGZvciBubyBJRCB0ZXN0"
    And Send POST request to /diffassign/v1/diff/"10020"/"right" endpoint with value "VGhpcyBpcyB0ZXN0IGZvciBubyBJRCB0ZXN0"
    When Send GET request to /diffassign/v1/diff/"NO_ID"/ endpoint
    Then Response must contain "200" status code
    And Response must contain "VGhpcyBpcyB0ZXN0IGZvciBubyBJRCB0ZXN0" in body
