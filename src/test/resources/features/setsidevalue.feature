@Sides
Feature: Test POST /diffassign/v1/diff/{id}/{side} endpoint

    # @Assumptions for test:
    # Min body value length = 4 chars (meets current implementation)
    # Max body value length = 64 chars (doesn't meet current implementation)
    # If value length exceeds allowed one, there should 400 status code and message "Data in body exceeds allowed length" about it

  @Smoke
  Scenario Outline: Send values to left and right side
    Given Send POST request to /diffassign/v1/diff/"<id>"/"<side>" endpoint with value "<value>"
    Then Response must contain "<result1>" and "<result2>" in body
    And Response must contain "<status_code>" status code
    Examples:
      | id | side  | value                | result1 | result2              | status_code |
      | 20 | left  | MIN_LENGTH           | left    | MIN_LENGTH           | 200         |
      | 21 | left  | MAX_LENGTH           | left    | MAX_LENGTH           | 200         |
      | 22 | right | MIN_LENGTH           | right   | MIN_LENGTH           | 200         |
      | 23 | right | MAX_LENGTH           | right   | MAX_LENGTH           | 200         |
      | 24 | left  | ONLY_NUMBERS         | left    | ONLY_NUMBERS         | 200         |
      | 25 | right | OnLyLetTersS         | right   | OnLyLetTersS         | 200         |
      # Updating values with same ID
      | 24 | right | WITH_ONE_EQUAL_SIGNS | right   | WITH_ONE_EQUAL_SIGNS | 200         |
      | 25 | left  | WITH_TWO_EQUAL_SIGNS | left    | WITH_TWO_EQUAL_SIGNS | 200         |
      # Empty values
      | 26 | left  |                      | left    |                      | 200         |
      | 27 | right  |                      | right   |                      | 200         |


  @Negative
  Scenario Outline: Send non-valid values to left and right side
    Given Send POST request to /diffassign/v1/diff/"<id>"/"<side>" endpoint with value "<value>"
    Then Response must contain "<result1>" and "<result2>" in body
    And Response must contain "<status_code>" status code
    Examples:
      | id        | side  | value             | result1         | result2                                                          | status_code |
      | 32        | right | MORE_THAN_ALLOWED | right           | Data in body exceeds allowed length                              | 400         |
      | 33        | left  | MORE_THAN_ALLOWED | left            | Data in body exceeds allowed length                              | 400         |
      | 34        | right | 12345             | "errorCode":415 | Data in body not Base64 formatted.                               | 415         |
      | non_digit | right | 1234              |                 |                                                                  | 404         |
      |           | left  | 1234              |                 |                                                                  | 404         |
      |           | right | 1234              |                 |                                                                  | 404         |
      | 35        |       | VGVz              |                 |                                                                  | 405         |
      | 36        | down  | VGVz              | "errorCode":501 | This side is not supported, please use either 'left' or 'right'. | 501         |
      | 37        | down  | VGVz              | "errorCode":501 | This side is not supported, please use either 'left' or 'right'. | 501         |
      | 38        | left  | NULL              | "errorCode":400 | Value in request body cannot be empty.                           | 400         |
      | 39        | left  | §!@#(*&%$«        | "errorCode":415 | Data in body not Base64 formatted.                               | 415         |


  @Negative
  Scenario: Send request not in JSON format
    Given Send POST request to /diffassign/v1/diff/"39"/"left" endpoint with value "VGVz" with "TEXT" Media-Type
    Then Response must contain "415" status code
    And Response must contain ""errorCode":415" and "Value in request body must be in JSON format." in body
