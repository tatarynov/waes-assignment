# WAES API tests
## Tech stack:
- Java 8 ([Install docs](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html))
- Maven ([Install docs](https://maven.apache.org/guides/))
- Cucumber (will be installed automatically)
- RestAssured (will be installed automatically)
- TestNG as test runner (will be installed automatically)
- AssertJ 
- Please see pom.xml file for more details on application modules


## How to set environment for running tests
Before executing tests there should be set next software:
- Java 8
- Maven


#### Tests parameters

Next properties could be used to run tests:
1. ```-Denv``` - set up host where API tests is running up. 
Available ptions: ```local, test, stage, prod```. 
Default value: ```http://localhost```

2. ```-Dcucumber.options='--tags @TagExample, @TagExample2``` -  set what tests should be run. 
Available options: ```@Smoke, @Sides, @Negative, etc.```

### Source code
Source code (git repo) is available at ```https://tatarynov@bitbucket.org/tatarynov/waes-assignment.git```


### Assumptions
- You already familiar with: Git, Java, Maven, Terminal/Command line implementation of your Operation System
- Application code will be located in (Unix): ```~/Projects``` or in (Windows) ```C:\Projects```

Use this command to download code base
```bash
cd PROJECT_DIR
git clone https://tatarynov@bitbucket.org/tatarynov/waes-assignment.git
```

Open terminal (Unix) or CMD prompt (MS Windows) and run following commands:
```bash
cd CLONED_PROJECT_DIR
```

## Run tests


Make sure that your application under test is running up and ready for testing.
Then run in directory with tests via command line/terminal:

```
mvn clean test
```
or with parameters:
```
mvn clean test -Dcucumber.options='--tags @Smoke'
```


## Reports and logs


Standard HTML report will be generated in ```./target/cucumber-report/index.html``` directory.

To generate advanced HTML report (e.g. to use in your pipeline), run: 
```
mvn cluecumber-report:reporting
```
Advanced HTML report will be generated in ```./target/generated-report/index.html``` directory.


_Example of advanced report_:

![alt text](https://tech.trivago.com/img/posts/cluecumber-report-plugin/overview.png)



